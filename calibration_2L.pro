PRO CALIBRATION_2L

for k=1,20 do begin
a = readfits('C:\VST_Simo_files\2l_II_quadrante\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\2_lenti\Fcal'+STRTRIM(k,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\2_lenti\Fcal'+STRTRIM(k,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\2_lenti\Fcal'+STRTRIM(k,2)+'.fits',c,h
endfor


for k=1,25 do begin
a = readfits('C:\VST_Simo_files\2l_I_quadrante\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\2_lenti\Fcal'+STRTRIM(k+20,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\2_lenti\Fcal'+STRTRIM(k+20,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\2_lenti\Fcal'+STRTRIM(k+20,2)+'.fits',c,h
endfor

END