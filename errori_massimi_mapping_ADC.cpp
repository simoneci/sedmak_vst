#include <iostream>
#include <fstream>
#include <cstdlib> // for exit function
#include <sstream>
#include "nrutil.h"

#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF2.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TApplication.h"
#include "TCanvas.h"

// #include "matpack.h"
// cd PSF_VST_model

// Compile with: 
/*g++ -o errori_massimi_mapping_ADC errori_massimi_mapping_ADC.cpp nrutil.c `rootlib` -ansi -O3 -Wall -L/usr/X11/lib -lm
*/



//     using namespace MATPACK;
    using namespace std;
/*
    using std::valarray;
    using Array::array1;
    using Array::array2; */

//   void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2);
  
  void SetSplineGlobalVar(string/*char* */,int,int/*,double,double*/);
  void splint(double[],double[],double[],int n,double,double*);
  void spline(double[],double[],int,double,double,double[]);
  void splie2(double[],double[],double**,int,int,double**);
  void splin2(double[],double[],double**,double**,int,int,double,double,double*);
  void SetSplineInterpolator2D(string/* char* */,int,int/*,double,double*/);
  double SplineInterpolator2D(double,double);

  int ndim = 81; 
  double **SecondDerivative;
  double **GlobalSplineMatrix;
  double *SplineX, *SplineY;
  int SplineDim[2];
  void SetSplineDim(int n1, int n2){SplineDim[0] = n1, SplineDim[1] = n2;}

int main(int argc, char* argv[]){
// Ad ognuno degli elementi delle psf deve essere associato un set di coordinate (x,y) 
TApplication app("gui",NULL,NULL);
TCanvas* c= new TCanvas();
c->Divide(2);	
TH2D *Ch1,*Ch2;

long nelements =9*9; //!!!!! 81 PSF

cout << "Scegli quale configurazione di ADC (0) oppure (50)" << endl;
Int_t scelta;
cin >> scelta;
cout << "ADC_" << scelta << endl;

stringstream zenith;

if(scelta==0) {
   zenith << scelta;
  }
  else if( scelta==50 ) {
   zenith << scelta;
  }
  else {
    cerr << "Scelta per la configurazione di ADC errata. Premi (0) o (50) !!!" << endl;
   exit(1);
  }

Int_t scelta_1;
std::cout<<"Mappatura degli rms dei massimi psf DATI-COMPRESSE " <<std::endl;
std::cout<<"Dimensioni immagini? (128x128)" << std::endl;
std::cout<<"Inserisci suffisso per directory e files: 'fit' oppure 'wrec'."<<std::endl;
std::cout<<"Per 'fit' premi (1); per 'wrec' premi (2)."<< std::endl;
std::cin>>scelta_1;

string suffisso;
if(scelta_1==1) {
   suffisso="fit";
  }
  else if( scelta_1==2 ) {
   suffisso="wrec";
  }
  else {
    cerr << "Scelta errata. Premi (1) o (2) !!!" << endl;
   exit(1);
  }

string nomefile("/mnt/e:/psf_" + suffisso + "_adc_"+zenith.str()+"/massimi_" + suffisso +".dat");
// char* nomefile = argv[1];// "massimi_wrec.dat" oppure "massimi_fit.dat"
ifstream inFile;
inFile.open(nomefile.c_str());
if (inFile.fail()) {
      cerr << "unable to open file " << nomefile << " for reading." << endl;
      exit(1);
  }

string nomefile_1("/mnt/e:/psf_cvl_adc_"+zenith.str()+"/massimi_cvl.dat");
ifstream inFile_1;
inFile_1.open(/*"input.dat"*/nomefile_1.c_str());
if (inFile_1.fail()) {
      cerr << "unable to open file " << nomefile_1 << " for reading." << endl;
      exit(1);
  }
// Cio' che serve e' il nome del file, le dimensioni della matrice i limiti sup. in x e y 

double *max_cvl = dvector(1,nelements);
double *max_cmp = dvector(1,nelements);

double invar;
for(Int_t j=1; j<=nelements; j++){ // 225 n� minimo di coeffs wavelet per PSF
  inFile>>invar;
  max_cmp[j] = invar;    
//   cout << "matrice= " << matrice[j][ii] << endl;
}


double invar_1;
for(Int_t j=1; j<=nelements; j++){ // 225 n� minimo di coeffs wavelet per PSF
  inFile_1>>invar_1;
  max_cvl[j] = invar_1;    
//   cout << "matrice= " << matrice[j][ii] << endl;
}


ofstream outdata;
if( !outdata ) { // file couldn't be opened
      cerr << "Errore: non resco ad aprire il file! Prova a premere 1 o 2 per la tua scelta!" << endl;
      exit(1);
}

string nomefile_2("/mnt/e:/psf_" + suffisso + "_adc_"+zenith.str()+"/err_[cvl-" + suffisso +"].dat");
cout << nomefile_2 << endl;;

outdata.open(nomefile_2.c_str());

double *err = dvector(1,nelements);
for(Int_t j=1; j<=nelements; j++){ 
  err[j] = max_cvl[j]-max_cmp[j];    
outdata << err[j] << endl;
}
outdata.close();
//! Poi apri file con gli errori e lo richiami!

ifstream inFile_3;
inFile_3.open(/*"input.dat"*/nomefile_2.c_str());
if (inFile_3.fail()) {
      cerr << "unable to open file " << nomefile_2 << " for reading." << endl;
      exit(1);
  }


SetSplineInterpolator2D(nomefile_2,9,9/*,.5,.5*/); 

// double valore(0.), valore1(0.);
// valore = SplineInterpolator2D(8000., 8000.);
// valore1 = SplineInterpolator2D(12000., 12000.);
// cout << "valore interpolato= " << valore << endl;
// cout << "valore interpolato1= " << valore1 << endl;

// Creare un TH2D e visualizzarne la forma che dovrebbe essere identica a quella dell'originale
  int nbx1 = 9; 
  int nby1 = 9;
  double xlow1 = 0.5; 
  double ylow1 = 0.5; 
  double xup1 = 16000.5; 
  double yup1 = 16000.5; 

  Ch1 = new TH2D("Ch1","Interpolazione 9x9",nbx1,xlow1,xup1,nby1,ylow1,yup1);
  
  // Questo e' l'istogramma da visualizzare by interpolation
  for(long i=1; i<=9/*250*/; i++){
  for(long j=1; j<=9/*250*/; j++){
  Ch1->SetBinContent(i,j,SplineInterpolator2D(i/9./*250.*/ *16000.,j/9./*250.*/*16000.));
  }
  }

   gStyle->SetPalette(1);
   c->cd(1);
  Ch1->SetFillColor(70);
  char* tipo = argv[1];
  Ch1->SetTitle(strcat(tipo," max errori, 9x9 mapping"));
  Ch1->GetXaxis()->SetTitle("X axis");
  Ch1->GetYaxis()->SetTitle("Y axis");
  Ch1->Draw("surf4");

  int nbx2 = 250;
  int nby2 = 250;

Ch2 = new TH2D("Ch2","Interpolazione 250x250",nbx2,xlow1,xup1,nby2,ylow1,yup1);
  
  // Questo e' l'istogramma da visualizzare by interpolation
  for(long i=1; i<=250; i++){
  for(long j=1; j<=250; j++){
  Ch2->SetBinContent(i,j,SplineInterpolator2D(i/250.*16000.,j/250.*16000.));
  }
  }

//    gStyle->SetPalette(1);
   c->cd(2);
  Ch2->SetFillColor(90);
  Ch2->SetTitle("250x250 mapping");
  Ch2->GetXaxis()->SetTitle("X axis");
  Ch2->GetYaxis()->SetTitle("Y axis");
  Ch2->Draw("surf4");


c->cd();
  app.Run(true);

return 0;
}

// Conflitto nrutil.c <---> root vvedi se riesci ad usare Matpack.... cambiare i dvctor e i dmatrix con Vector e Matrix
////////////////////////////////////////////////////////////////////////////////////
  //Setting the global variables for Spline Interpolation
  void SetSplineGlobalVar(string namefile, int n1, int n2/*, double bmax1, double bmax2*/){
    ndim = n1*n2;
    int i;//, j, k;
    double *splinef = new double[ndim];

    double **splinematrix = dmatrix(1,n1,1,n2);

    double *xx = dvector(1,n1);
    double *yy = dvector(1,n2);

    for(int i=1; i<=n1; i++) xx[i] = (double)i*1600.;//double(2*i-(n1+1))/double(n1-1)*bmax1;      
    SplineX = xx;
    for(int i=1; i<=n2; i++) yy[i] = (double)i*1600.;//double(2*i-(n2+1))/double(n2-1)*bmax2;      
    SplineY = yy;
    ifstream in(namefile.c_str());
    double invar;
    i = 0;
    while(in>>invar){
      splinef[i] = invar;
    i++;
    }

    for(int i=0; i<9; i++)
    cout << "splinef " << splinef[i] << endl; 

    for(int i=0;i<n1;i++){
      for(int j=0;j<n2;j++){
	splinematrix[i+1][j+1] = splinef[i*n2+j/*n1*(n2-1-j)+i*/];
      }      
    }


    for(int i=1; i<=9; i++)
    cout << "SplineX " << SplineX[i] << endl; 

    for(int i=1; i<=9; i++)
    cout << "SplineY " << SplineY[i] << endl; 

//     for(int i=0;i<n1;i++){
//       for(int j=0;j<n2;j++){
//       cout << "splinematrix("<< i+1<<","<< j+1<<")="<< splinematrix[i+1][j+1] << endl;
//       }      
//     }
     GlobalSplineMatrix = splinematrix;

     cout << "GlobalSplinematrix[9][19] " << GlobalSplineMatrix[9][9] << endl;
  }

void splint(double xa[], double ya[], double y2a[], int n, double x, double *y){
    int klo,khi,k;
    double h,b,a;
    klo=1;		// We will find the right place in the table by means of bisection.
    khi=n;		// This is optimal if sequential calls to this routine are at 
    while (khi-klo > 1) {	// random values of x. If sequential calls are in order, 
      k=(khi+klo) >> 1;	// and closely spaced, one would do better to store the
      if (xa[k] > x) khi=k;	// previous values of klo and khi and test if they
      else klo=k;			// remain appropriate on the next call.
    }						// klo and khi now bracket the input value of x.
    h=xa[khi]-xa[klo];
    try{if (h == 0.0) {
	throw "Bad xa input to routine splint";
      }
    }
    // This error can occur only if two input xa's are (to within roundoff) identical
    catch (char* message) {
      cout << message;
    }
    a=(xa[khi]-x)/h;
    b=(x-xa[klo])/h;
    *y=a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
  }
////////////////////////////////////////////////////////////////////////////////////
//Given arrays xa[1..n] and ya[1..n], with x1 < x2 < ... < xn, and given  
//values of yp1 and ypn for the first derivative of the interpolating function
//at points 1 and n, respectively, this routine returns an array y2[1..n]
//that contains the second derivatives of the interpolating function at the
//tabulated points xi. If yp1 an/or ypn are equal to 1 x 10^30 or larger, the
//routine is signaled to set the corresponding boundary condition for a
//natural spline, with zero second derivative on that bounday.
////////////////////////////////////////////////////////////////////////////////////
  void spline(double x[], double y[], int n, double yp1, double ypn, double y2[]){
    int i,k;
    double p,qn,sig,un,*u;
    u=dvector(1,n-1);
    if (yp1 > 0.99e30)		// The lower boundary condition is set either to be
      y2[1]=u[1]=0.0;		// "natural" or else to have a specified first 
    else {					// derivative.
      y2[1] = -0.5;
      u[1]=(3.0/(x[2]-x[1]))*((y[2]-y[1])/(x[2]-x[1])-yp1);
    }
    for (i=2;i<=n-1;i++) {	// This is the decomposition loop of the tridiagonal
      sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);	// algorithm. y2 and u are used for
      p=sig*y2[i-1]+2.0;		// temporary storage of the decomposed factors.
      y2[i]=(sig-1.0)/p;
      u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
      u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
    }
    if (ypn > 0.99e30)		// The lower boundary condition is set either to be
      qn=un=0.0;			// "natural" or else to have a specified first
    else {					// derivative.
      qn=0.5;
      un=(3.0/(x[n]-x[n-1]))*(ypn-(y[n]-y[n-1])/(x[n]-x[n-1]));
    }
    y2[n]=(un-qn*u[n-1])/(qn*y2[n-1]+1.0);
    for (k=n-1;k>=1;k--)	// This is the backsubstitution loop of the tridiagonal 		
      y2[k]=y2[k]*y2[k+1]+u[k];		//algorithm
      free_dvector(u,1,n-1);
    }
////////////////////////////////////////////////////////////////////////////////////
  void splie2(double x1a[], double x2a[], double **ya, int m, int n, double **y2a){
    int j;
    for(j=1;j<=m;j++) spline(x2a,ya[j],n,1.0e30,1.0e30,y2a[j]);
  }
////////////////////////////////////////////////////////////////////////////////////
  void splin2(double x1a[], double x2a[], double **ya, double **y2a, int m, int n, 
	      double x1, double x2, double *y){
    int j; 
    double *ytmp, *yytmp;
    ytmp = dvector(1,m);
    yytmp = dvector(1,m);
    for(j=1;j<=m;j++) splint(x2a,ya[j],y2a[j],n,x2,&yytmp[j]);
    spline(x1a,yytmp,m,1.e30,1.e30,ytmp);
    splint(x1a,yytmp,ytmp,m,x1,y);
    free_dvector(yytmp,1,m);
    free_dvector(ytmp,1,m);
  }
////////////////////////////////////////////////////////////////////////////////////
  void SetSplineInterpolator2D(string namefile, int n1, int n2/*, double bmax1, double bmax2*/){
    SetSplineDim(n1,n2);
    SetSplineGlobalVar(namefile,SplineDim[0],SplineDim[1]/*,bmax1,bmax2*/);
    double **derivative = dmatrix(1,n1,1,n2); 
    splie2(SplineX,SplineY,GlobalSplineMatrix,SplineDim[0],SplineDim[1],derivative);
    SecondDerivative = derivative;
  }
////////////////////////////////////////////////////////////////////////////////////
//Before callinf SplineInterpolator2D SetSplineInterpolator2D has to be called
  double SplineInterpolator2D(double x1, double x2){
    double SplineValue;
    if(x1<=SplineX[1]) x1 = SplineX[1];
    if(x2<=SplineY[1]) x2 = SplineY[1];
    if(x1>=SplineX[SplineDim[0]]) x1 = SplineX[SplineDim[0]];
    if(x2>=SplineY[SplineDim[1]]) x2 = SplineY[SplineDim[1]];
    splin2(SplineX,SplineY,GlobalSplineMatrix,SecondDerivative,
	   SplineDim[0],SplineDim[1],x1,x2,&SplineValue);
    return SplineValue;
  }