/* Get_Wave_parameters_81 prende i tutti gli 81 files contenenti i 1500 parametri di compressione wavelet e li converte PER OGNI PARAMETRO, in una matrice di 9x9 che e' la mappa del parametro (1500 files di 9x9 valori) al variare delle coordinate */

#include <iostream>
#include <fstream>
#include <cstdlib> // for exit function
#include <sstream>
#include <cstdio>
#include <string>
#include "nrutil.h"

#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF2.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TApplication.h"
#include "TCanvas.h"

// #include "matpack.h"
// cd PSF_VST_model

// Compile with: 
/*g++ -o Get_Wave_parameters_81 Get_Wave_parameters_81.cpp nrutil.c `rootlib` -lm

*/



//     using namespace MATPACK;
    using namespace std;

int main(int argc, char* argv[]){
// Ad ognuno degli elementi delle psf deve essere associato un set di coordinate (x,y) 


cout<<"----Organizzazione dei coefficienti wavelet delle PSF in formato .DAT " <<endl;
// Prova con PSF.dat ci sono le coordinate ed i corresp. valori di intensita'
cout<<"per mapping dei coefficienti.----"<<endl;
cout << endl;

int scelta;

std::cout<<"Dimensioni immagini? (128x128)" << std::endl;
// std::cout<<"Configurazione 2 LENTI"<<std::endl;
std::cout<<"Inserisci suffisso per la configurazione: (2 lenti) oppure ADC"<<std::endl;
std::cout<<"Per (2 lenti) premi (1); per ADC_0 premi (2); per ADC_50 (3)."<< std::endl;
std::cin>>scelta;

string suffisso;
if(scelta==1) {
   suffisso="2l";
  }
  else if( scelta==2 ) {
   suffisso="adc_0";
  }
  else if( scelta==3 ) {
   suffisso="adc_50";
  }
  else {
    cerr << "Scelta errata. Premi (1) o (2) o (3) !!!" << endl;
   exit(1);
  }

// Vogliamo una matrice 20*100
double **matrice = dmatrix(1,1550,1,100);
//double *vettore = dvector(1,20);
double invar;

for(Int_t ii=1; ii<=81; ii++){
// Int_t ii=1;
ifstream inFile;
stringstream conversione;
conversione<<ii;
string nomefile = "/mnt/e:/psf_wrec_"+suffisso+"/c_non_zero_"+ conversione.str() + ".dat"; 
// string nomefile = "./data/prms_G4_"+ conversione.str() + ".dat";

inFile.open(nomefile.c_str());
if (inFile.fail()) {
   cerr << "Unable to open file " << nomefile << " for reading." << endl;
   exit(1);
  }

for(Int_t j=1; j<=1550; j++){ // 225 n� minimo di coeffs wavelet per PSF
  inFile>>invar;
  matrice[j][ii] = invar;    
//   cout << "matrice= " << matrice[j][ii] << endl;
}
}

cout << endl;


for(Int_t k=1; k<=1550; k++){
ofstream outdata;
stringstream converter;

converter<<k;
string nome = "/mnt/e:/psf_wrec_"+suffisso+"/cw_cmp_" + converter.str() + ".dat"; //coeff. wavelet
if( !outdata ) { // file couldn't be opened
      cerr << "Error: file could not be opened" << endl;
      exit(1);
}
outdata.open(nome.c_str());

for(Int_t i=1; i<=81; i++){
outdata << matrice[k][i] << endl;
// outdata.close();
}

outdata.close();
}

return 0;
}
