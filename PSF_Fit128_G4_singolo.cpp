// PSF_Fit_128.cpp
// Author: Simone Carozza
// 09/07/2006

/* Fit Non lineare di PSF osservative (telescopio*seeing) con 3 gaussiane per polinomi di 6,5,4� grado in 2D */
/* Immagini (128x128), Fit sicuramente + rapido; seeing con una gaussiana */


#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF2.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TApplication.h"
#include "TCanvas.h"

#include "TGraphDelaunay.h"
#include "TVirtualPad.h"
#include "TVirtualFitter.h"

#include <iostream>
#include <cstdio>
#include <CCfits>
#include <cmath>
// The library CCfits is enclosed in a namespace.

#include "matpack.h"
#include "Array.h" // For using Array class performing fftw++ FFT
//#include "fftw++.h"

/*  Compile: g++ -o PSF_Fit128_G4_singolo PSF_Fit128_G4_singolo.cpp -I ~/tri -lCCfits `rootlib` -I/opt/matpack/include -DXPM_INCLUDE="<X11/xpmh>" -ansi -O3 -Wall -L/usr/X11/lib -lXpm -lX11 -lmatpack -lm */

// fftw++.h in conflitto con MatPAck
    using namespace CCfits;
    using namespace MATPACK;
    using namespace std;

    using std::valarray;
    //using Array::array1;
    using Array::array2; 

void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2);
void twodim_to_onedim(Matrix mat, std::valarray<double>& arr, long s1, long s2);
void find_xMax_yMax(Matrix mat, Double_t max, long s1, long s2);
Int_t get_xMax(Matrix mat, Double_t max, long s1, long s2);
Int_t get_yMax(Matrix mat, Double_t max, long s1, long s2);

int main();
int NonlinearFit();

int main()
{
     FITS::setVerboseMode(true);


   try
   {
      if (!NonlinearFit()) std::cerr << " NonlinearFit() \n";
   }
   catch (FitsException&)
   
   {
      std::cerr << " Fits Exception Thrown by test function \n";
   }
  return 0;
}
  
Double_t g_poly_4(Double_t *x, Double_t *par) {
   Double_t r1 = Double_t((x[0]-par[1])/par[2]);
   Double_t r2 = Double_t((x[1]-par[3])/par[4]);
   Double_t u = Double_t(x[0]-par[1]);
   Double_t v = Double_t(x[1]-par[3]);
   return par[0]*TMath::Exp(-0.5*(r1*r1+r2*r2))*(par[5]+par[6]*u+par[7]*v+par[8]*u*v+par[9]*u*u+par[10]*v*v+
   par[11]*u*u*v+par[12]*u*v*v+par[13]*pow(u,3)+par[14]*pow(v,3)+par[15]*pow(u,3)*v+par[16]*pow(v,3)*u+par[17]*pow(u,2)*pow(v,2)+par[18]*pow(u,4)+par[19]*pow(v,4));
} 


int NonlinearFit(/*int argc, char* argv[]*/){

//! Peppino

// for(Int_t k=1; k<=100; k++){
// Qui ci andrebbe un bel loop su tutte le  psf convolute
//   stringstream converter;
// //   Int_t k = 1;
//   converter << k ; // lo stesso per scrittura su file dei params

//gStyle->SetOptFit(0011);
// TApplication app("gui",NULL,NULL); // Questo non ci servira' +
// TCanvas* c= new TCanvas();	
// c->Divide(2,2);

TH2D *Ch1,*Ch1_Fit;
TF2* PSF_fit(0);


string nomefile;
// string nomefile = "psf_cvl_" + converter.str() + ".fits";
long naxis    =   2;      
    long naxes[2] = {128,128};

std::cout<<"Fit non lineare di PSF osservativa in formato .FITS " <<std::endl;
std::cout<<"Dimensioni immagini? (128x128)" << std::endl;
std::cout<<"File .FITS: "<<std::endl;
std::cin>>nomefile;
std::cout<<"Input file:"<<nomefile << endl;

// Per il collaudo dell'algoritmo: usa una immagine convoluta: PSF_Seeing_convolved.fits 5/09/2006
   std::auto_ptr<FITS> pInfile0(new FITS("/mnt/e:/"+ nomefile/*"psf_prova_Gauss_convolved.fits"*/,Read,true));

    long nelements;
        
        PHDU& image0 = pInfile0->pHDU(); 
        std::valarray<double>  contents0; 
	image0.read(contents0);
	
        long ax1_0(image0.axis(0));
        long ax2_0(image0.axis(1));

        std::cout << "ax1_0: " << ax1_0 << std::endl;
        std::cout << "ax2_0: " << ax2_0 << std::endl;

        nelements = std::accumulate(&naxes[0], &naxes[naxis],1,std::multiplies<long>());

size_t align=sizeof(Complex);
        
array2<double> C0_2D(ax1_0,ax2_0,align); 
	
onedim_to_twodim(contents0,C0_2D,ax1_0,ax2_0);

Matrix Data(1,ax1_0,1,ax2_0); // Questi mi servono per conoscere il massimo ed il minimo valore dei dati
  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  Data[i][j]=C0_2D[j-1][i-1];   // Qui facciamo il cambio j->x-->i; i->y-->j.
  }
  }

Double_t Data_Max = Max(Data);
// Double_t Data_Min = Min(Data);

// cout << "Max dati= " << Data_Max << endl;
// cout << "Min dati= " << Data_Min << endl;

find_xMax_yMax(Data, Data_Max, ax1_0, ax2_0);
Int_t x_Max = get_xMax(Data, Data_Max, ax1_0, ax2_0);
Int_t y_Max = get_yMax(Data, Data_Max, ax1_0, ax2_0);

// cout << "x_Max= " << x_Max << ", y_Max= " << y_Max << endl;

// create histogram for fitting 

  int nbx1 = 128;
  int nby1 = 128;
  double xlow1 = 0.; 
  double ylow1 = 0.; 
  double xup1 = 128.; // Reference coords changing for each PSF
  double yup1 = 128.; 

  Ch1 = new TH2D("Ch1","PSF dati",nbx1,xlow1,xup1,nby1,ylow1,yup1);
  
  for(long i=1; i<=ax2_0; i++){
  for(long j=1; j<=ax2_0; j++){
  Ch1->SetBinContent(i,j,Data[i][j]/*C0_2D[i-1][j-1]*/); 
  }
  }

  // Primo indice: ordinata; Secondo indice: ascissa.
  // Taglia Ch1 in x e y, -> 2 istogrammi -> sigma_x, sigma_y
  TH1D *Ch1_x, *Ch1_y;
  Ch1_x = new TH1D("Ch1_x","PSF dati lungo y fisso (y_Max)",nbx1,xlow1,xup1);
  for(long i=1; i<=ax1_0; i++){

  Ch1_x->SetBinContent(i,Ch1->GetBinContent(i,y_Max)); 
  }

//   cout << "Ch1_x(y_Max)= " << Ch1_x->GetBinContent(y_Max) << endl;
//   cout << "Ch1_x_Sigma=" << Ch1_x->GetRMS() << endl; // Non serve per il Fit

  
  Ch1_y = new TH1D("Ch1_y","PSF dati lungo x fisso (x_Max)",nby1,ylow1,yup1);
  for(long j=1; j<=ax2_0; j++){
  Ch1_y->SetBinContent(j,Ch1->GetBinContent(x_Max,j)); 
  }

//   cout << "Ch1_y(x_Max)= " << Ch1_y->GetBinContent(x_Max) << endl;
//   cout << "Ch1_y_Sigma=" << Ch1_y->GetRMS() << endl; // Non serve per il Fit
  

//   gStyle->SetPalette(1);
//   c->cd(1);
//   Ch1->GetXaxis()->SetTitle("X axis");
//   Ch1->GetYaxis()->SetTitle("Y axis");
//   Ch1->Draw("surf4");


  const Int_t npar = 5; // 5 sono i parametri della gaussiana da inizzializzare
  Int_t npar1 = 0;
  Double_t funzparams[npar] = 
    {Data_Max,x_Max,Ch1_x->GetRMS()/2,y_Max,Ch1_y->GetRMS()/2};

  cout << "Modello a 1 g. modulata da 1 polinomio di 4�, centro g. fluttuante." << endl;


  // Proviamo a fare il fit con gauss*poly; 20 sono i params del polinomio di 4� grado da inizializzare (=1.)
  npar1 = 20; 
  Double_t funzparams1[npar1];
  for(Int_t i=0; i<5; i++)
  {
  funzparams1[i]=funzparams[i];
  }
  for(Int_t i=5; i<19; i++)
  {
  funzparams1[i]=1.;
  }
  funzparams1[19]=0.;

  cout << endl;
  
  for(Int_t i=0; i<20; i++)
  {
  cout << "funzparams1[" << i << "]= " << funzparams1[i] << endl;
  }


  cout << endl;
  
  PSF_fit = new TF2("PSF_fit", g_poly_4 ,0.,128.,0.,128.,npar1); 
  PSF_fit->SetParameters(funzparams1);
//! In questo caso non fisso parametri


//    c->cd(2);
  //gStyle->SetOptFit(0010); 
  //gStyle->SetPalette(1);
  //funzione->SetFillColor(55);
//   PSF_fit->SetFillColor(55);
  //Ch1->Fit("funzione","R");
  Ch1->Fit("PSF_fit","R");
//   PSF_fit->SetTitle("fit non lineare di PSF osservativa");
//   PSF_fit->GetXaxis()->SetTitle("X axis");
//   PSF_fit->GetYaxis()->SetTitle("Y axis");
//   //funzione->Draw("surf4");
//   PSF_fit->Draw("surf4");
//   //dt->Draw("same p0");
   
cout << endl;
// Recupero dei parametri e scrittura su file
/*int  fprintf (FILE * stream , const char * format [ , argument , ...] );
  Print formatted data to a stream.*/
//   FILE * outfile;
//   outfile = fopen ("psf_prms_G4.dat","w");
// 
//   Double_t pr(0.);
//   for(Int_t p=0; p<npar1; p++){
//   pr = PSF_fit->GetParameter(p);
//   cout << "parametro(" << p << ")= " << pr << endl;
//   fprintf(outfile,"%e \n",pr);
//   }
// 
//   cout << endl;
//   fclose(outfile);

  // Qui ci andrebbe un bel loop

//   stringstream converter;
//   Int_t k = 1;
//   converter << k ;
  string nomefile1;
  ofstream outdata;
  nomefile1 = "prms_G4_recovered.dat";
  outdata.open(nomefile1.c_str()/*"psf_prms_G4.dat"*/); // opens the file
   if( !outdata ) { // file couldn't be opened
      cerr << "Error: file could not be opened" << endl;
      exit(1);
   }
  for (Int_t i=0; i<npar1; ++i)
      outdata << PSF_fit->GetParameter(i) << endl;
   outdata.close();

  // Normalizzazione a int. 1 (Volume) dei dati di uscita del fit
  Matrix Ch1_Data_Fit(1,ax1_0,1,ax2_0);
  for(long i=1; i<=ax1_0;i++){
  for(long j=1; j<=ax2_0;j++){
  Ch1_Data_Fit[i][j]=PSF_fit->Eval(i,j); 
  }
  }

   // histogr. dati_Fit da visualizzare pre-normalizz.
  Ch1_Fit = new TH2D("Ch1_Fit","PSF_fit_data",nbx1,xlow1,xup1,nby1,ylow1,yup1);
  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  Ch1_Fit->SetBinContent(i,j,Ch1_Data_Fit[i][j]); 
  }
  }

  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  if(Ch1_Fit->GetBinContent(i,j)<0.){
  cout << "Fitting data < 0." << endl;
//   cout << Ch1_Fit->GetBinContent(i,j) << endl;
  cout << "Fitting data ("<< i << "," <<  j << ")= " << Ch1_Fit->GetBinContent(i,j) << endl;
  Ch1_Fit->SetBinContent(i,j,0.);
  Ch1_Data_Fit[i][j]=0.;
  }
  }
  }


// Devo vedere il fit previa normalizzazione
  Double_t norm(0.), norm1(0.);
  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  norm+=Ch1_Data_Fit[i][j];
  }
  }
  
//! Errore su fit previa normalizzazione 
  /*cout << "norma dati di uscita del fit pre_normalizzazione= " << norm << endl;
  cout << endl;

 
  cout << "Massimo d'intensita' dei dati= "<< Ch1->GetBinContent(x_Max,y_Max) << endl;
  Double_t max =  Ch1_Data_Fit[x_Max][y_Max]; 
  // E' opportuno usare MatPack per Max Dati e coords xMax, yMax
  cout << "Valore dati best fit in corr. massimo dati pre_norm= "  << max << endl;
  cout << "Errore picco dati - dati best fit in corr. coords picco dati - (relativo): " <<
  (Data_Max- max)/Data_Max << endl;
  cout << "Errore picco dati - dati best fit in corr. coords picco dati - (percentuale): " <<
  TMath::Abs((Data_Max - max)/Data_Max)*100 << "%"<< endl;
  cout << endl;
  cout << "Massimo d'intensita' dei dati best fit e relative coordinate:" << endl;*/
  Double_t Ch1_Fit_Max = Max(Ch1_Data_Fit);
 /* cout << "Ch1_Fit_max= " << Ch1_Fit_Max << endl;
  
  find_xMax_yMax(Ch1_Data_Fit, Ch1_Fit_Max, ax1_0, ax2_0);
  cout << "Errore picco dati - picco dati best fit - (relativo): " << (Data_Max - Ch1_Fit_Max)/ Data_Max << endl;
  cout << "Errore picco dati - picco dati best fit - (percentuale): " << 
  TMath::Abs((Data_Max - Ch1_Fit_Max)/ Data_Max)*100 << "%"<< endl;
  cout << endl;

  TH2D *Ch1_Fit_Peak;

  Ch1_Fit_Peak = new TH2D("Ch1_Fit_Peak","PSF_fit_data norm picco",nbx1,xlow1,xup1,nby1,ylow1,yup1);
  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  Ch1_Fit_Peak->SetBinContent(i,j,Ch1_Data_Fit[i][j]*(Data_Max/Ch1_Fit_Max)); 
  }
  }
  
  Matrix Ch1_Data_Fit_Peak(1,ax1_0,1,ax2_0);
  for(long i=1; i<=ax1_0;i++){
  for(long j=1; j<=ax2_0;j++){
  Ch1_Data_Fit_Peak[i][j]=Ch1_Fit_Peak->GetBinContent(i,j); 
  }
  }*/
  
//   cout << endl;
//   cout << "Dati best fit normalizzati a picco fit" << endl;
//   Double_t max_peak =  Ch1_Data_Fit_Peak[x_Max][y_Max]; 
//   cout << "Errore picco dati - dati best fit in corr. coords picco dati - (relativo): " <<
//   (Data_Max- max_peak)/Data_Max << endl;
//   cout << "Errore picco dati - dati best fit in corr. coords picco dati - (percentuale): " <<
//   TMath::Abs((Data_Max - max_peak)/Data_Max)*100 << "%"<< endl;
//   cout << endl;
//   cout << "Massimo d'intensita' dei dati best fit e relative coordinate:" << endl;
//   Double_t Ch1_Fit_Max_Peak = Max(Ch1_Data_Fit_Peak);
//   cout << "Ch1_Fit_max_Peak= " << Ch1_Fit_Max_Peak << endl;
//   
//   find_xMax_yMax(Ch1_Data_Fit_Peak, Ch1_Fit_Max_Peak, ax1_0, ax2_0);
//   cout << "Errore picco dati - picco dati best fit - (relativo): " << (Data_Max - Ch1_Fit_Max_Peak)/ Data_Max << endl;
//   cout << "Errore picco dati - picco dati best fit - (percentuale): " << 
//   TMath::Abs((Data_Max - Ch1_Fit_Max_Peak)/ Data_Max)*100 << "%"<< endl;
//   cout << endl;
  
  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  Ch1_Data_Fit[i][j]=Ch1_Data_Fit[i][j]/norm;
  }
  }
  
  for(long i=1; i<=ax1_0; i++){
  for(long j=1; j<=ax2_0; j++){
  norm1+=Ch1_Data_Fit[i][j];
  } 
  }  

  cout << endl;

//   c->cd(3);
//   Ch1_Fit->SetFillColor(70);
//   Ch1_Fit->SetTitle("PSF normalizzata dati fittati");
//   Ch1_Fit->GetXaxis()->SetTitle("X axis");
//   Ch1_Fit->GetYaxis()->SetTitle("Y axis");
//   Ch1_Fit->Draw("surf4"); 
// //   c->cd();
// 
//   // visualizzazione di dati e fit_dati sovrapposti per confronto
//   c->cd(4);
//     const Int_t n = ax1_0;
//    Double_t x[n], xf[n];
//    Double_t y[n], yf[n];
//    for(Int_t i=1; i<=n; i++){
//   x[i] = i;
//   y[i] = Ch1_x->GetBinContent(i); 
// //   printf(" i %i %f %f \n",i,x[i],y[i]);
//   xf[i] = i;
//   yf[i] = Ch1_Data_Fit[i][y_Max];
// //   printf(" i_fit %i %f %f \n",i,xf[i],yf[i]);
//    }
// 
//    TGraph* gr, * gr_fit; 
//    gr = new TGraph(n,x,y);
//    gr_fit = new TGraph(n,xf,yf);
//    gr->SetLineColor(4);
// //    gr->SetLineWidth(4);
//    gr->SetMarkerColor(3);
// //    gr->SetMarkerStyle(21);
//    gr->SetTitle("Sovrapposizione dati / dati fittati");
//    gr->GetXaxis()->SetTitle("X title");
//    gr->GetYaxis()->SetTitle("Y title");
//    gr->Draw("AC*");
//    gr_fit->SetLineColor(2);
//    gr_fit->SetLineWidth(2);
// //    gr_fit->SetMarkerColor(7);
//    gr_fit->SetMarkerStyle(21);
//    gr_fit->Draw("CP");
// 
//   cout << endl;
// 
//   // Dati di picco ed errori relativi
//   cout << "norma dati di uscita del fit previa normalizzazione= " << norm << endl;
//   cout << endl;
// 
//   cout << "norma dati di uscita del fit normalizzati= " << norm1 << endl;
//   cout << endl; 
//  
//   cout << "Massimo d'intensita' dei dati= "<< Ch1->GetBinContent(x_Max,y_Max) << endl;
//   max =  Ch1_Data_Fit[x_Max][y_Max]; 
//   // E' opportuno usare MatPack per Max Dati e coords xMax, yMax
//   cout << "Valore dati best fit in corr. massimo dati post-norm= "  << max << endl;
//   cout << "Errore picco dati - dati best fit in corr. coords picco dati - (relativo): " <<
//   (Data_Max- max)/Data_Max << endl;
//   cout << "Errore picco dati - dati best fit in corr. coords picco dati - (percentuale): " <<
//   TMath::Abs((Data_Max - max)/Data_Max)*100 << "%"<< endl;
//   cout << endl;
  cout << "Massimo d'intensita' dei dati best fit e relative coordinate:" << endl;
  Ch1_Fit_Max = Max(Ch1_Data_Fit);
  cout << "Ch1_Fit_max= " << Ch1_Fit_Max << endl;
//   
  find_xMax_yMax(Ch1_Data_Fit, Ch1_Fit_Max, ax1_0, ax2_0);
  cout << "Errore picco dati - picco dati best fit - (relativo): " << (Data_Max - Ch1_Fit_Max)/ Data_Max << endl;
  cout << "Errore picco dati - picco dati best fit - (percentuale): " << 
  TMath::Abs((Data_Max - Ch1_Fit_Max)/ Data_Max)*100 << "%"<< endl;
  cout << endl;
// 
//   //Errore relativo al picco per dare un'idea della bonta' del fit...
//   // RMSE : variazione dei residui per altro test sulla bonta' del fit
//   Double_t MSE=0.;
//   Double_t RMSE;//,fitness_RMSE;
//   for(long i=1; i<=ax1_0; i++)
//   for(long j=1; j<=ax2_0; j++) 
//   MSE+=pow(Ch1->GetBinContent(i,j)-Ch1_Data_Fit[i][j],2);
//   
//   cout << "MSE= " << MSE << endl;
// 
//   RMSE=sqrt(MSE/nelements); // Non e' indicativo, l'errore e' piccolo perche' i numeri sono piccoli
//   cout << "RMSE= " << RMSE << endl; 

//    c->cd();
  std::valarray<double> psf(nelements);
  twodim_to_onedim(Ch1_Data_Fit,psf,ax1_0,ax2_0);
	
  std::cout<<std::endl;

  std::auto_ptr<FITS> pFits(0);
       
  string outfits = "!psf_fit_recovered.fits";

  try{
      const std::string fileN(outfits);
      pFits.reset( new FITS(fileN , DOUBLE_IMG , naxis , naxes ) );
      }
      catch (FITS::CantCreate){
      return -1; 
      }		
  long  fpixel(1);
  pFits->pHDU().write(fpixel,nelements,psf); 

delete Ch1; 
delete Ch1_x; 
delete Ch1_y; 
delete Ch1_Fit;

// }
//   app.Run(true);

return 0;
}

// E' opportuno fare un file functs.cpp dove si implementano queste funzioni per poterle riutilizzare
void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2){
long offset(0);
while(offset<s1*s2-s2){
for(long i=0;i<s1;i++){
for(long j=0;j<s2;j++){
arr2[i][j]=arr[j+offset];
}offset+=s2;
}
}
}

void twodim_to_onedim(Matrix mat, std::valarray<double>& arr, long s1, long s2){
// long offset(0);
for(long i=0;i<s1;i++){
for(long j=0;j<s2;j++){
arr[i*s2+j]=mat[j+1][i+1]; // Scambio i<-->j ; x<-->y
}//offset+=s2;
}
}

void find_xMax_yMax(Matrix mat, Double_t max, long s1, long s2){
Double_t xm,ym;
  for(long i=1; i<=s1;i++){
  for(long j=1; j<=s2;j++){
  if(mat[i][j]==max){
  xm=i;
  ym=j;
  cout<<"(xMax,yMax)= " << "(" << xm << ","<< ym << ")" << endl;
  break;
  }
  }
  } 
}

Int_t get_xMax(Matrix mat, Double_t max, long s1, long s2){
Int_t xm(0);
  for(long i=1; i<=s1;i++){
  for(long j=1; j<=s2;j++){
  if(mat[i][j]==max){
  xm=i;
  break;
  cout << "xMax= " << xm << endl;
  }
  }
  }
return xm; 
}

Int_t get_yMax(Matrix mat, Double_t max, long s1, long s2){
Int_t ym(0);
  for(long i=1; i<=s1;i++){
  for(long j=1; j<=s2;j++){
  if(mat[i][j]==max){
  ym=j;
  break;
  cout << "yMax= " << ym << endl;
  }
  }
  }
return ym; 
}
