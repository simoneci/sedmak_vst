#include <iostream>
#include <fstream>
#include <cstdlib> // for exit function
#include <sstream>
#include <cstdio>
#include <string>

#include <CCfits>
#include <cmath>
// The library CCfits is enclosed in a namespace.
// Compile with: g++ -o From_dat_to_FITS_wavelet From_dat_to_FITS_wavelet.cpp -I ~/tri -lCCfits -lm

// #include "Array.h" // For using Array class performing fftw++ FFT

  using namespace CCfits;
  using namespace std;
  using std::valarray;

int main(){

 FITS::setVerboseMode(true);

  long naxis    =   2;      
  long naxes[2] = {128,128};

  long nelements = naxes[0]*naxes[1];

cout << "Scegli quale ADC (0) oppure (50)." << endl;
int scelta;
cin >> scelta;
cout << "ADC_" << scelta << endl;
stringstream zenith; 



if(scelta==0) {
   zenith << scelta;
  }
  else if( scelta==50 ) {
   zenith << scelta;
  }
  else {
    cerr << "Scelta errata. Premi (0) o (50) !!!" << endl;
   exit(1);
  }

for(long ii=1; ii<=81; ii++){
// long ii=95;
ifstream inFile;
stringstream conversione;
conversione<<ii;
string nomefile = "/mnt/e:/psf_wrec_adc_"+zenith.str()+"/psf_wrec_"+ conversione.str() + ".dat"; 
// string nomefile = "./data/prms_G4_"+ conversione.str() + ".dat";

inFile.open(nomefile.c_str());
if (inFile.fail()) {
   cerr << "Unable to open file " << nomefile << " for reading." << endl;
   exit(1);
  }

std::valarray<double> contents(nelements);
double invar;

for(long j=0; j<nelements; j++){ 
  inFile>>invar;
  contents[j] = invar;
}

for (long j = 63; j < 64; j++){
	       std::ostream_iterator<double> Anti(std::cout << " riga[" << j << "]" << std::endl, "\t");                std::copy(&contents[j*128],&contents[(j+1)*128-1],Anti);
            std::cout << std::endl;
            }

std::auto_ptr<FITS> pWrec(0);
       
string outfits = "!/mnt/e:/psf_wrec_adc_"+zenith.str()+"/psf_wrec_"+ conversione.str() +".fits";

       try{
          const std::string fileName(outfits);
	  pWrec.reset( new FITS(fileName, DOUBLE_IMG , naxis , naxes ) );
       }
	  catch (FITS::CantCreate){
      return -1;                                                                                                                 }	
      long  fpixel(1);
      pWrec->pHDU().write(fpixel,nelements,contents); 

}
return 0;
}