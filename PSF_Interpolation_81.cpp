#include <iostream>
#include <fstream>
#include <cstdlib> // for exit function
#include <sstream>
#include <cstdio>
#include <string>
#include "nrutil.h"

#include "TROOT.h"
#include "TMath.h"
#include "TStyle.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF2.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TApplication.h"
#include "TCanvas.h"


// Compile with: 
/*g++ -o PSF_Interpolation_81 PSF_Interpolation_81.cpp nrutil.c `rootlib` -lm
*/

    using namespace std;

  void SetSplineGlobalVar(const char*,int,int/*,double,double*/);
  void splint(double[],double[],double[],int n,double,double*);
  void spline(double[],double[],int,double,double,double[]);
  void splie2(double[],double[],double**,int,int,double**);
  void splin2(double[],double[],double**,double**,int,int,double,double,double*);
  void SetSplineInterpolator2D(const char*,int,int/*,double,double*/);
  double SplineInterpolator2D(double,double);

  int ndim = 81; // 81 PSF
  double **SecondDerivative;
  double **GlobalSplineMatrix;
  double *SplineX, *SplineY;
  int SplineDim[2];
  void SetSplineDim(int n1, int n2){SplineDim[0] = n1, SplineDim[1] = n2;}

int main(int argc, char* argv[]){


cout<<"----Interpolazione dei parametri delle PSF fit modello in formato .DAT " <<endl;
// Prova con PSF.dat ci sono le coordinate ed i corresp. valori di intensita'
cout<<"alle coordinate desiderate.----"<<endl;

//! Distinzione tra interpolazione per "wavelets" e modello "gauss*polyn"

ofstream outfile;
double valore;


//! If 1 Fit; if 2 wavelet

int scelta,scelta_1;
std::cout<<"Scegli tipo di dati da interpolare: 81 parametri di fit o 1550 coefficienti wavelet;"<<std::endl;
std::cout<<"Per (fit) premi (1); per wavelet premi (2)."<< std::endl;
std::cin>>scelta;
std::cout<<"Inserisci suffisso per la configurazione: (2 lenti) oppure ADC"<<std::endl;
std::cout<<"Per (2 lenti) premi (1); per ADC_0 premi (2); per ADC_50 (3)."<< std::endl;
std::cin>>scelta_1;

string suffisso;
if(scelta_1==1) {
   suffisso="2l";
  }
  else if( scelta_1==2 ) {
   suffisso="adc_0";
  }
  else if( scelta_1==3 ) {
   suffisso="adc_50";
  }
  else {
    cerr << "Scelta errata. Premi (1) o (2) o (3) !!!" << endl;
   exit(1);
  }

double x,y;
cout<<"Inserisci coordinate di interpolazione desiderate "<<endl; 
cout<<"(1670. < x < 16.000, 1670. < y < 16.000):"<<endl;
cout<<"x?"<<endl;
cin>>x;
cout<<"y?"<<endl;
cin>>y;
cout<<"x = "<<x<<" , y= "<<y<<" \n";
// double valore;


if(scelta==1){
cout<<"interpolazione dei parametri di fit."<<endl;
// ofstream outfile;
// double valore;
string nome_0("/mnt/e:/psf_fit_"+suffisso+"/psf_interp.dat");

outfile.open(nome_0.c_str());
outfile<<x<<endl;
outfile<<y<<endl;
for(Int_t k=0; k<20; k++){

stringstream converter;
converter << k;
string nome;
nome = "/mnt/e:/psf_fit_"+suffisso+"/p_" + converter.str() + ".dat";

 SetSplineInterpolator2D(nome.c_str()/*"./data/p_1.dat"*/,9,9);
// double valore(0.);
valore = SplineInterpolator2D(x, y);
cout << "parametro interpolato p" << k  <<" = "<<  valore << endl;
outfile << valore << endl;
}
outfile.close();
}

//! Distinzione tra interpolazione per 2 lenti o adc
if(scelta==2){
string nomefile("/mnt/e:/psf_wrec_"+suffisso+"/wave_interpolation.dat");
outfile.open(nomefile.c_str());
outfile<<x<<endl;
outfile<<y<<endl;

for(Int_t k=1; k<=1550; k++){

stringstream converter;
converter << k;
string nome;
nome = "/mnt/e:/psf_wrec_"+suffisso+"/cw_cmp_" + converter.str() + ".dat";

 SetSplineInterpolator2D(nome.c_str()/*"./data/p_1.dat"*/,9,9);

valore = SplineInterpolator2D(/*2200., 2200.*/x,y);
cout << "parametro interpolato w" << k  <<" = "<<  valore << endl;
outfile << valore << endl;
}
outfile.close();
}
// app.Run(true);

return 0;
}

  //Setting the global variables for Spline Interpolation
  void SetSplineGlobalVar(const char* namefile, int n1, int n2/*, double bmax1, double bmax2*/){
    ndim = n1*n2;
    int i;//, j, k;
    double *splinef = new double[ndim];

    double **splinematrix = dmatrix(1,n1,1,n2);

    double *xx = dvector(1,n1);
    double *yy = dvector(1,n2);

    for(int i=1; i<=n1; i++) xx[i] = (double)i*1600.;//double(2*i-(n1+1))/double(n1-1)*bmax1;      
    SplineX = xx;
    for(int i=1; i<=n2; i++) yy[i] = (double)i*1600.;//double(2*i-(n2+1))/double(n2-1)*bmax2;      
    SplineY = yy;
    ifstream in(namefile);
    double invar;
    i = 0;
    while(in>>invar){
      splinef[i] = invar;
    i++;
    }

//     for(int i=0; i<4; i++)
//     cout << "splinef " << splinef[i] << endl; 

    for(int i=0;i<n1;i++){
      for(int j=0;j<n2;j++){
	splinematrix[i+1][j+1] = splinef[i*n2+j/*n1*(n2-1-j)+i*/];
      }      
    }


    for(int i=1; i<=2; i++)
    cout << "SplineX " << SplineX[i] << endl; 

    for(int i=1; i<=2; i++)
    cout << "SplineY " << SplineY[i] << endl; 

//     for(int i=0;i<n1;i++){
//       for(int j=0;j<n2;j++){
// //       cout << "splinematrix("<< i+1<<","<< j+1<<")="<< splinematrix[i+1][j+1] << endl;
//       }      
//     }
     GlobalSplineMatrix = splinematrix;

     cout << "GlobalSplinematrix[2][2] " << GlobalSplineMatrix[2][2] << endl;
  }

void splint(double xa[], double ya[], double y2a[], int n, double x, double *y){
    int klo,khi,k;
    double h,b,a;
    klo=1;		// We will find the right place in the table by means of bisection.
    khi=n;		// This is optimal if sequential calls to this routine are at 
    while (khi-klo > 1) {	// random values of x. If sequential calls are in order, 
      k=(khi+klo) >> 1;	// and closely spaced, one would do better to store the
      if (xa[k] > x) khi=k;	// previous values of klo and khi and test if they
      else klo=k;			// remain appropriate on the next call.
    }						// klo and khi now bracket the input value of x.
    h=xa[khi]-xa[klo];
    try{if (h == 0.0) {
	throw "Bad xa input to routine splint";
      }
    }
    // This error can occur only if two input xa's are (to within roundoff) identical
    catch (char* message) {
      cout << message;
    }
    a=(xa[khi]-x)/h;
    b=(x-xa[klo])/h;
    *y=a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
  }
////////////////////////////////////////////////////////////////////////////////////
//Given arrays xa[1..n] and ya[1..n], with x1 < x2 < ... < xn, and given  
//values of yp1 and ypn for the first derivative of the interpolating function
//at points 1 and n, respectively, this routine returns an array y2[1..n]
//that contains the second derivatives of the interpolating function at the
//tabulated points xi. If yp1 an/or ypn are equal to 1 x 10^30 or larger, the
//routine is signaled to set the corresponding boundary condition for a
//natural spline, with zero second derivative on that bounday.
////////////////////////////////////////////////////////////////////////////////////
  void spline(double x[], double y[], int n, double yp1, double ypn, double y2[]){
    int i,k;
    double p,qn,sig,un,*u;
    u=dvector(1,n-1);
    if (yp1 > 0.99e30)		// The lower boundary condition is set either to be
      y2[1]=u[1]=0.0;		// "natural" or else to have a specified first 
    else {					// derivative.
      y2[1] = -0.5;
      u[1]=(3.0/(x[2]-x[1]))*((y[2]-y[1])/(x[2]-x[1])-yp1);
    }
    for (i=2;i<=n-1;i++) {	// This is the decomposition loop of the tridiagonal
      sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);	// algorithm. y2 and u are used for
      p=sig*y2[i-1]+2.0;		// temporary storage of the decomposed factors.
      y2[i]=(sig-1.0)/p;
      u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
      u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
    }
    if (ypn > 0.99e30)		// The lower boundary condition is set either to be
      qn=un=0.0;			// "natural" or else to have a specified first
    else {					// derivative.
      qn=0.5;
      un=(3.0/(x[n]-x[n-1]))*(ypn-(y[n]-y[n-1])/(x[n]-x[n-1]));
    }
    y2[n]=(un-qn*u[n-1])/(qn*y2[n-1]+1.0);
    for (k=n-1;k>=1;k--)	// This is the backsubstitution loop of the tridiagonal 		
      y2[k]=y2[k]*y2[k+1]+u[k];		//algorithm
      free_dvector(u,1,n-1);
    }
////////////////////////////////////////////////////////////////////////////////////
  void splie2(double x1a[], double x2a[], double **ya, int m, int n, double **y2a){
    int j;
    for(j=1;j<=m;j++) spline(x2a,ya[j],n,1.0e30,1.0e30,y2a[j]);
  }
////////////////////////////////////////////////////////////////////////////////////
  void splin2(double x1a[], double x2a[], double **ya, double **y2a, int m, int n, 
	      double x1, double x2, double *y){
    int j; 
    double *ytmp, *yytmp;
    ytmp = dvector(1,m);
    yytmp = dvector(1,m);
    for(j=1;j<=m;j++) splint(x2a,ya[j],y2a[j],n,x2,&yytmp[j]);
    spline(x1a,yytmp,m,1.e30,1.e30,ytmp);
    splint(x1a,yytmp,ytmp,m,x1,y);
    free_dvector(yytmp,1,m);
    free_dvector(ytmp,1,m);
  }
////////////////////////////////////////////////////////////////////////////////////
  void SetSplineInterpolator2D(const char* namefile, int n1, int n2/*, double bmax1, double bmax2*/){
    SetSplineDim(n1,n2);
    SetSplineGlobalVar(namefile,SplineDim[0],SplineDim[1]/*,bmax1,bmax2*/);
    double **derivative = dmatrix(1,n1,1,n2); 
    splie2(SplineX,SplineY,GlobalSplineMatrix,SplineDim[0],SplineDim[1],derivative);
    SecondDerivative = derivative;
  }
////////////////////////////////////////////////////////////////////////////////////
//Before callinf SplineInterpolator2D SetSplineInterpolator2D has to be called
  double SplineInterpolator2D(double x1, double x2){
    double SplineValue;
    if(x1<=SplineX[1]) x1 = SplineX[1];
    if(x2<=SplineY[1]) x2 = SplineY[1];
    if(x1>=SplineX[SplineDim[0]]) x1 = SplineX[SplineDim[0]];
    if(x2>=SplineY[SplineDim[1]]) x2 = SplineY[SplineDim[1]];
    splin2(SplineX,SplineY,GlobalSplineMatrix,SecondDerivative,
	   SplineDim[0],SplineDim[1],x1,x2,&SplineValue);
    return SplineValue;
  }