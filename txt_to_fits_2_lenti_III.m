clear all
close all

n = 512;
sc = 0.214/2;

%--- elenco psf ottiche 2 lenti - 17/11/2006
%--- III quadr in alto a dx

elenco = {...
      'D:\2l_III_quadrante\VST_2l_-0.13_-0.52.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.26_-0.52.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.39_-0.52.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.52_-0.52.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.13_-0.39.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.26_-0.39.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.39_-0.39.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.52_-0.39.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.13_-0.26.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.26_-0.26.TXT',... 
      'D:\2l_III_quadrante\VST_2l_-0.39_-0.26.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.52_-0.26.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.13_-0.13.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.26_-0.13.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.39_-0.13.TXT',...
      'D:\2l_III_quadrante\VST_2l_-0.52_-0.13.TXT'...
    
}

% skip = [18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,...
%      18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0]

for i = 1:length(elenco)


a = reshape(dlmread(elenco{i},'\t',18,0),512,512);

%figure; imagesc(a);axis image

outname = strcat('C:\VST_Simo_files\2l_III_quadrante\F',num2str(i),'.fits')
fitswrite(a,outname);


end


% --- read fits file of zemax telescope psf (512 x 512 pixel, 0.214 arcsec/pixel) 

% --- 0.13_0.52
t = fitsread(char('C:\VST_Simo_Files\2l_III_quadrante\F1.fits'));
tt = sum(sum(t));
psf_tel = t/tt;

figure; imagesc(psf_tel,'Xdata',sc*[-n/2,n/2-1],'Ydata',sc*[-n/2,n/2-1]); axis image;
title('optical psf'); xlabel('arcsec'); ylabel('arcsec')

% --- 0.52_0.52
t = fitsread(char('C:\VST_Simo_Files\2l_III_quadrante\F4.fits'));
tt = sum(sum(t));
psf_tel = t/tt;

figure; imagesc(psf_tel,'Xdata',sc*[-n/2,n/2-1],'Ydata',sc*[-n/2,n/2-1]); axis image;
title('optical psf'); xlabel('arcsec'); ylabel('arcsec')

% --- 0.13_0.13
t = fitsread(char('C:\VST_Simo_Files\2l_III_quadrante\F13.fits'));
tt = sum(sum(t));
psf_tel = t/tt;

figure; imagesc(psf_tel,'Xdata',sc*[-n/2,n/2-1],'Ydata',sc*[-n/2,n/2-1]); axis image;
title('optical psf'); xlabel('arcsec'); ylabel('arcsec')

% --- 0.13_0.52
t = fitsread(char('C:\VST_Simo_Files\2l_III_quadrante\F16.fits'));
tt = sum(sum(t));
psf_tel = t/tt;

figure; imagesc(psf_tel,'Xdata',sc*[-n/2,n/2-1],'Ydata',sc*[-n/2,n/2-1]); axis image;
title('optical psf'); xlabel('arcsec'); ylabel('arcsec')
