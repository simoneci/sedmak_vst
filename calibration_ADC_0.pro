PRO CALIBRATION_ADC_0


for k=1,4 do begin
a = readfits('C:\VST_Simo_files\ADC_IV_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k,2)+'.fits',c,h
endfor


for k=1,5 do begin
a = readfits('C:\VST_Simo_files\ADC_I_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+4,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+4,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+4,2)+'.fits',c,h
endfor


for k=5,8 do begin
a = readfits('C:\VST_Simo_files\ADC_IV_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+5,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+5,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+5,2)+'.fits',c,h
endfor

for k=6,10 do begin
a = readfits('C:\VST_Simo_files\ADC_I_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+8,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+8,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+8,2)+'.fits',c,h
endfor

for k=9,12 do begin
a = readfits('C:\VST_Simo_files\ADC_IV_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+10,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+10,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+10,2)+'.fits',c,h
endfor

for k=11,15 do begin
a = readfits('C:\VST_Simo_files\ADC_I_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+12,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+12,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+12,2)+'.fits',c,h
endfor

for k=13,16 do begin
a = readfits('C:\VST_Simo_files\ADC_IV_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+15,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+15,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+15,2)+'.fits',c,h
endfor

for k=16,20 do begin
a = readfits('C:\VST_Simo_files\ADC_I_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+16,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+16,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+16,2)+'.fits',c,h
endfor

for k=17,20 do begin
a = readfits('C:\VST_Simo_files\ADC_IV_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+20,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+20,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+20,2)+'.fits',c,h
endfor


for k=21,25 do begin
a = readfits('C:\VST_Simo_files\ADC_I_quad\F'+STRTRIM(k,2)+'.fits')

b=rebin(a,64,64)
c=fltarr(128,128)
c(32:95,32:95)=b(*,*)
mkhdr,h,a
mkhdr,h,b
mkhdr,h,c
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+20,2)+'.fits',a,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+20,2)+'.fits',b,h
writefits,'C:\VST_Simo_cal\ADC_0\Fcal'+STRTRIM(k+20,2)+'.fits',c,h
writefits
endfor


END