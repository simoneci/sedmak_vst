// Seeing_convolution.cpp: derivato da "prova_Gauss_convolve.cpp"
// Author: Simone Carozza
// 09/05/2006

/* Con il progr Seeing_convolution.cpp si legge in INPUT un file .FITS, in particolare psf_ray_tracing(i,j;i,j) ZEMAX
    e  psf_seeing(i,j;i,j) e farne la convoluzione in modo da ottenere psf_osservativa(i,j;i,j) sempre in formato .FITS 
come  OUTPUT */
/* Il seeing viene approssimato da 1 gaussiana !!!! */

//! Opportuno inserire la giusta sigma delle gaussiane approssimanti il seeing, singoli fov dai files Zemax processati
// file ZEMAX processato in dir VST_Simo_cal: data_space
 
//! Immagini in formato 128x128

#include <iostream>
#include <fstream>
#include <cstdlib> // for exit function
#include <sstream>
#include <CCfits>
#include <cmath>
// The library CCfits is enclosed in a namespace.

#include "Array.h" // For using Array class performing fftw++ FFT
#include "fftw++.h"
// Compile with: g++ -o Seeing_convolution_ADC Seeing_convolution_ADC.cpp -I ~/tri fftw++.cc -lfftw3 -lCCfits    

    using namespace CCfits;
    using namespace std;

    using std::valarray;
    using Array::array1;
    using Array::array2;

void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2);
void onedim_to_twodim/*_complex*/(array1<Complex> arr, array2<Complex> arr2, long s1, long s2);
void twodim_to_onedim(array2<double> arr2, std::valarray<double>& arr, long s1, long s2); /* passi std::valarray per referenza !!! */
void twodim_to_onedim(array2<Complex> arr2, array1<Complex> arr, long s1, long s2);
void Complex_product(array1<Complex> arrin_a, array1<Complex> arrin_b, array1<Complex> arr_out, long spy);
void Shift(array2<double> arr2, long s1, long s2); // It works only with odd n1,n2 
void Set_Seeing(array2<double> arr2, double xc, double yc, long s1, long s2,/*double amp2,*/ double scala);
void Normalize_Psf(array2<double> arr2,long s1,long s2);

int main();
int images_Convolution();

int main()
{
     FITS::setVerboseMode(true);


   try
   {
      if (!images_Convolution()) std::cerr << " images_Convolution() \n";
   }
   catch (FitsException&)
   
   {
      std::cerr << " Fits Exception Thrown by test function \n";
   }
  return 0;
}


int images_Convolution(){

cout << "Scegli quale ADC (0) oppure (50)" << endl;
int scelta;
cin >> scelta;
cout << "ADC_" << scelta << endl;
stringstream zenith; 



if(scelta==0) {
   zenith << scelta;
  }
  else if( scelta==50 ) {
   zenith << scelta;
  }
  else {
    cerr << "Scelta errata. Premi (0) o (50) !!!" << endl;
   exit(1);
  }

std::cout<<"Convoluzioni di immagini in formato .FITS con seeing numerico approssimato." <<std::endl;

for(long k=1; k<=81; k++){
ifstream inFile;
stringstream converter;
// int k = 2;
converter<<k;
string file1 = "/mnt/e:/VST_Simo_cal/adc_"+zenith.str()+"/Fcal" + converter.str() + ".fits";

long naxis    =   2;      
    long naxes[2] = {128,128};

std::cout<<"File .FITS da convolvere:"<< file1 << std::endl;
// std::cin>>file1;

   std::auto_ptr<FITS> pInfile0(new FITS(file1/*"psf2.fits"*/,Read,true)); // file Zemax Fcal#.FITS processato


    long nelements;
        
        PHDU& image0 = pInfile0->pHDU(); 
        std::valarray<double>  contents0; // valarray<float/double>
	image0.read(contents0);
        
// 	std::cout << "come e' fatto image0 ?" << endl;
//         std::cout << "sizeof(image0) = " << sizeof(image0) << std::endl; 
// 	
        long ax1_0(image0.axis(0));
        long ax2_0(image0.axis(1));

//         std::cout << "ax1_0: " << ax1_0 << std::endl;
//         std::cout << "ax2_0: " << ax2_0 << std::endl;

        nelements = std::accumulate(&naxes[0], &naxes[naxis],1,std::multiplies<long>());
	
// 	std::cout << "images0 nelements = " << nelements << endl;
//         std::cout << std::endl;


double xc = ax1_0/2;/*+0.333+2;//128/2; // PSF::SetCenter();*/
double yc = ax2_0/2;/*+0.1713+3;//128/2;*/

// std::cout << "centro della gaussiana bidimensionale in: (xc,yc) = (" << xc << "," << yc << ")" << std::endl; 

//! Tener conto del fov dei files Zemax per la scala
cout << endl;

double data_area(180.75); 
double scala(0.);
// cout << "Inserisci data_area per calcolo Sigma Seeing"<< endl;
// cin >> data_area;

// cout << "data_area= "<< data_area << endl;
scala= 128.*0.65/(2.*(data_area/15.)*0.214);
// cout << "Sigma= "<< scala << endl;
// cout << endl;

array2<double> gaussiana_2D(ax1_0,ax2_0,sizeof(Complex)); // ya lista para la fft

array2<double> seeing_2D(ax1_0,ax2_0,sizeof(Complex));

Set_Seeing(seeing_2D, xc, yc, ax1_0,ax2_0,scala);

Normalize_Psf(seeing_2D,ax1_0,ax2_0);

double integrale_1(0.);
for(long i=0; i<ax1_0; i++)
for(long j=0; j<ax2_0; j++)
integrale_1+=seeing_2D[i][j];

// std::cout << "integrale_1= " << integrale_1 << std::endl;
std::cout << std::endl; 

// 1D zzazione del seeing per scrittura su file .FITS
std::valarray<double> sg(nelements);
twodim_to_onedim(/*gaussiana*/seeing_2D,sg,ax1_0,ax2_0);


std::auto_ptr<FITS> pFitsSeeing(0);
       
       try{
          const std::string fileName2("!psf_Seeing_ADC_" + zenith.str() +".fits");
	  pFitsSeeing.reset( new FITS(fileName2 , DOUBLE_IMG , naxis , naxes ) );
       }
	  catch (FITS::CantCreate){
      return -1;                                                                                                                 }	
      long  fpixel(1);
      pFitsSeeing->pHDU().write(fpixel,nelements,sg); 


	size_t align=sizeof(Complex);
//	long np = nelements/2+1;
        long axp=ax1_0/2+1;
//	std::cout << "np = " << np << std::endl; 
        array2<double> N0_2D(ax1_0,ax2_0,align); // array2<float/double> 
    //    array2<double> N1_2D(ax1_0,ax2_0,align);
	
	int tot(0);
// 	std::cout << "nelements(16384)-ax2_0(128) = " << nelements-ax2_0 << std::endl;
	
onedim_to_twodim(contents0,N0_2D,ax1_0,ax2_0);

    // ffts di contents0 e gaussiana2D:
        array2<Complex> N0_2Dfft(ax1_0,axp,align);
	array2<Complex> G1_2Dfft(ax1_0,axp,align);
	rcfft2d Forward_N0_2D(N0_2D,N0_2Dfft);
	rcfft2d Forward_N1_2D(/*gaussiana*/seeing_2D,G1_2Dfft);
	
        Forward_N0_2D.fft(N0_2D,N0_2Dfft);
        Forward_N1_2D.fft(/*gaussiana*/seeing_2D,G1_2Dfft);

long nels=ax1_0*axp;
// std::cout << "nels(8192)-axp(65) = " << nels-axp << std::endl;

array1<Complex> N0_1Dfft(nels,align);
array1<Complex> G1_1Dfft(nels,align);

twodim_to_onedim(N0_2Dfft,N0_1Dfft,ax1_0,axp);
twodim_to_onedim(G1_2Dfft,G1_1Dfft,ax1_0,axp);

// Finita la 1d zzazione delle _2Dfft
// Moltiplicazione nel Complex delle 1Dfft
// 2D zzazione del prodotto (-> N0convN1_2D )e 2D(anti)fft
array1<Complex> N0convG1_1D(nels,align);

Complex_product(N0_1Dfft,G1_1Dfft,N0convG1_1D,nels);

array2<Complex> N0convG1_2D(ax1_0,axp,align);

onedim_to_twodim(N0convG1_1D,N0convG1_2D,ax1_0,axp);

std::cout<<std::endl;

    // Anti-fft di N0convN1   
     array2<double> AntiN0convG1_2D(ax1_0,ax2_0,align);
	
	crfft2d Backward_N0convG1_2D(ax1_0,ax2_0,N0convG1_2D,AntiN0convG1_2D);
	
       Backward_N0convG1_2D.fft(N0convG1_2D,AntiN0convG1_2D);

Shift(AntiN0convG1_2D,ax1_0,ax2_0);
// PSF::Shift();

Normalize_Psf(AntiN0convG1_2D,ax1_0,ax2_0);

integrale_1=0.0;
for(long i=0; i<ax1_0; i++)
for(long j=0; j<ax2_0; j++)
integrale_1+=AntiN0convG1_2D[i][j];

std::valarray<double> AntiN0convG1(nelements);

twodim_to_onedim(AntiN0convG1_2D,AntiN0convG1,ax1_0,ax2_0);

     for (long j = 64; j < 65; j++){
	       std::ostream_iterator<double> Anti(std::cout << " riga[" << j << "]" << std::endl, "\t");                std::copy(&AntiN0convG1[j*128],&AntiN0convG1[(j+1)*128-1],Anti);
            std::cout << std::endl;
            }  
// AntiN0convG1 dovrebbe essere il prodotto di convoluzione...
// Adesso bisogna riconvertire array1<double> in std::valarray<double>
// cosa da poter scrivere l'imm. .fits	
      std::cout<<std::endl;

      std::auto_ptr<FITS> pFitsAnti(0);
       
       try{
          const std::string fileName3("!/mnt/e:/psf_cvl_adc_"+ zenith.str() +"/psf_cvl_"+ converter.str() +".fits");
	  pFitsAnti.reset( new FITS(fileName3 , DOUBLE_IMG , naxis , naxes ) );
       }
	  catch (FITS::CantCreate){
      return -1;                                                                                                                 }		

      pFitsAnti->pHDU().write(fpixel,nelements,/*Anticontents*/AntiN0convG1); 
}
	return 0;
}

void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2){
long offset(0);
while(offset<s1*s2-s2){
for(long i=0;i<s1;i++){
for(long j=0;j<s2;j++){
arr2[i][j]=arr[j+offset];
}offset+=s2;
}
}
}

void onedim_to_twodim(array1<Complex> arr, array2<Complex> arr2, long s1, long s2){
long offset(0);
while(offset<s1*s2-s2){
for(long i=0;i<s1;i++){
for(long j=0;j<s2;j++){
arr2[i][j]=arr[j+offset];
}offset+=s2;
}
}
}

void twodim_to_onedim(array2<Complex> arr2, array1<Complex> arr, long s1, long s2){
long offset(0);
while(offset<s1*s2-s2){
for(long i=0;i<s1;i++){
for(long j=0;j<s2;j++){
arr[j+offset]=arr2[i][j];
}offset+=s2;
}
}
}

void twodim_to_onedim(array2<double> arr2, std::valarray<double>& arr, long s1, long s2){
int offset(0);
while(offset<s1*s2-s2){
for(int i=0;i<s1;i++){
for(int j=0;j<s2;j++){
arr[j+offset]=arr2[i][j];
}offset+=s2;
}
}
}

void Complex_product(array1<Complex> arrin_a, array1<Complex> arrin_b, array1<Complex> arr_out, long spy){
for(unsigned int i=0; i < spy; i++) // PSF::complexFFTmult();
       arr_out[i]=Complex(arrin_a[i].real()*arrin_b[i].real()-arrin_a[i].imag()*arrin_b[i].imag(),arrin_a[i].real()*arrin_b[i].imag()+arrin_a[i].imag()*arrin_b[i].real());
} 

void Shift(array2<double> arr2, long s1, long s2){
// FFT row and column dimensions might be different
double *tmp13, *tmp24;
tmp13 = new double;
tmp24 = new double;

long s1_2 = s1 / 2 ; //half of row dimension
long s2_2 = s2 / 2 ; // half of column dimension

// interchange entries in 4 quadrants, 1 <--> 3 and 2 <--> 4

for (long i = 0; i < s1_2; i++)
{
for (long k = 0; k < s2_2; k++)
{
*tmp13 = arr2[i][k];
arr2[i][k] = arr2[i+s1_2][k+s2_2];
arr2[i+s1_2][k+s2_2] = *tmp13;

*tmp24 = arr2[i+s1_2][k];
arr2[i+s1_2][k] = arr2[i][k+s2_2];
arr2[i][k+s2_2] = *tmp24;
}
}
delete tmp13,tmp24;
}

void Set_Seeing(array2<double> arr2, double xc, double yc, long s1, long s2,/*double amp2,*/ double scala){
double sigma_x = scala/2.0; 
double sigma_y = scala/2.0;
for(long i=0; i<s1; i++)    
for(long j=0; j<s2; j++)
arr2[i][j]=exp(-0.5*(pow((i-xc),2)/pow(sigma_x,2)+pow((j-yc),2)/pow(sigma_y,2)))/*+0.01*exp(-0.5*(pow((i-xc),2)/pow(10.*sigma_x,2)+pow((j-yc),2)/pow(10.*sigma_y,2)))*/;
}

void Normalize_Psf(array2<double> arr2,long s1,long s2){
double integrale(0.);
for(long i=0; i<s1; i++)    
for(long j=0; j<s2; j++)
integrale+=arr2[i][j];
for(long i=0; i<s1; i++)    
for(long j=0; j<s2; j++)
arr2[i][j]=arr2[i][j]/integrale;
}