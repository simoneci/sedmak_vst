// Author: Simone Carozza
// 11/08/2006

/* Con il progr ASTRO_2D_multidata128.cpp si calcolano i centroidi delle psf, rms, massimi; psf osservative (cvl ovvero convolute), psf ricostruite con modello di fit (fit), psf ricostruite con wavelet (wrec) */


#ifdef _MSC_VER
#include "MSconfig.h" // for truncation warning
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "TROOT.h"

#include <iostream>
#include <cstdio>
#include <CCfits>
#include <cmath>
// The library CCfits is enclosed in a namespace.

#include "matpack.h"
#include "Array.h" // For using Array class performing fftw++ FFT
//#include "fftw++.h"
// Compile with: 
/*g++ -o ASTRO_2D_multidata128 ASTRO_2D_multidata128.cpp `rootlib` -I ~/tri -lCCfits -I/opt/matpack/include -DXPM_INCLUDE="<X11/xpmh>" -ansi -O3 -Wall -L/usr/X11/lib -lXpm -lX11 -lmatpack -lm 
*/


    using namespace CCfits;
    using namespace MATPACK;
    using namespace std;

    using std::valarray;
    using Array::array1;
    using Array::array2; 

void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2);
Double_t get_xCentroid(Matrix mat, long s1, long s2);
Double_t get_yCentroid(Matrix mat, long s1, long s2);

int main();
int Find_Centroid();

int main()
{
     FITS::setVerboseMode(true);


   try
   {
      if (!Find_Centroid()) std::cerr << " Find_Centroid() \n";
   }
   catch (FitsException&)
   
   {
      std::cerr << " Fits Exception Thrown by test function \n";
   }
  return 0;
}

//! Astrometria dati

int Find_Centroid(){
string nomefile;
// int scelta; // per distinguere directories e files di output

long naxis    =   2;      
    long naxes[2] = {128,128};

std::cout<<"Astrometria di immagini in formato .FITS " <<std::endl;
std::cout<<"Dimensioni immagini? (128x128)" << std::endl;
std::cout<<"Configurazione 2 LENTI"<<std::endl;
// std::cout<<"Inserisci suffisso per directory e files: cvl oppure fit oppure wrec"<<std::endl;
// std::cout<<"Per cvl premi (1); per fit premi (2); per wrec premi (3)."<< std::endl;
// std::cin>>scelta;


cout << "Scegli quale configurazione di ADC (0) oppure (50)" << endl;
Int_t scelta;
cin >> scelta;
cout << "ADC_" << scelta << endl;

stringstream zenith;

if(scelta==0) {
   zenith << scelta;
  }
  else if( scelta==50 ) {
   zenith << scelta;
  }
  else {
    cerr << "Scelta per la configurazione di ADC errata. Premi (0) o (50) !!!" << endl;
   exit(1);
  }

Int_t scelta_1; // per distinguere directories e files di output
std::cout<<"Inserisci suffisso per directory e files: cvl oppure fit oppure wrec"<<std::endl;
std::cout<<"Per cvl premi (1); per fit premi (2); per wrec premi (3)."<< std::endl;
std::cin>>scelta_1;

string suffisso;
if(scelta_1==1) {
   suffisso="cvl";
   cout << "files tipo " << suffisso;
  }
  else if( scelta_1==2 ) {
   suffisso="fit";
   cout << "files tipo " << suffisso;
  }
  else if( scelta_1==3 ) {
   suffisso="wrec";
   cout << "files tipo " << suffisso;
  }
  else {
    cerr << "Scelta tipo files errata. Premi (1) o (2) o (3) !!!" << endl;
   exit(1);
  }

ofstream outdata;
if( !outdata ) { // file couldn't be opened
      cerr << "Errore: premi 1 o 2 o 3 per la tua scelta!" << endl;
      exit(1);
}

// suffisso = "cvl";
// string suffisso_1 = "zozzo";
string file_0("/mnt/e:/psf_" + suffisso + "_adc_"+zenith.str()+"/errori_" + suffisso +".dat");
cout << file_0;

outdata.open(file_0.c_str()/*"/mnt/e:/psf_" + suffisso + "/errori_" + suffisso +".dat"*/); //! unisci stringhe con stringa_in: "cvl" o "wrec" o "fit"

string file_1("/mnt/e:/psf_" + suffisso + "_adc_" +zenith.str()+"/centroidi_" + suffisso +".dat");
 FILE * pFile;
 pFile = fopen (file_1.c_str()/*"/mnt/e:/psf_" + suffisso + "/centroidi_"<< suffisso +".dat"*/,"w");

ofstream outdata_1;
string file_2("/mnt/e:/psf_"+ suffisso +"_adc_"+zenith.str()+"/massimi_"+ suffisso +".dat");
if( !outdata_1 ) { // file couldn't be opened
      cerr << "Error: file could not be opened" << endl;
      exit(1);
}
outdata_1.open(file_2.c_str()/*"/mnt/e:/psf_"<< suffisso <<"/massimi_"<< suffisso <<".dat"*/);

for(Int_t ii=1; ii<=81; ii++){
//std::cin>>nomefile;
ifstream inFile;
stringstream conversione;
conversione<<ii;
nomefile = "/mnt/e:/psf_"+suffisso +"_adc_"+zenith.str()+"/psf_"+ suffisso +"_"+ conversione.str() + ".fits"; //! unisci stringhe con stringa_in: "cvl" o "wrec"
std::cout<<nomefile<<std::endl;


   std::auto_ptr<FITS> pInfile0(new FITS(nomefile,Read,true));
  // std::auto_ptr<FITS> pInfile1(new FITS(nomefile1,Read,true));
    long nelements;
        
        PHDU& image0 = pInfile0->pHDU(); 
        std::valarray<double>  contents0; 
	image0.read(contents0);
	
        long ax1_0(image0.axis(0));
        long ax2_0(image0.axis(1));

        std::cout << "ax1_0: " << ax1_0 << std::endl;
        std::cout << "ax2_0: " << ax2_0 << std::endl;

        nelements = std::accumulate(&naxes[0], &naxes[naxis],1,std::multiplies<long>());

size_t align=sizeof(Complex);
        
array2<double> C0_2D(ax1_0,ax2_0,align); 
// array2<double> C1_2D(ax1_0,ax2_0,align); 
	
onedim_to_twodim(contents0,C0_2D,ax1_0,ax2_0);
// onedim_to_twodim(contents1,C1_2D,ax1_0,ax2_0);

Matrix Dati(1,ax1_0,1,ax2_0);
for(long i=1; i<=ax1_0;i++){
for(long j=1; j<=ax2_0;j++){
Dati[i][j]=C0_2D[j-1][i-1]; // Scambio x<-->y !!!
}
}


cout<<endl;
cout << "Studio dell'errore dei centroidi delle PSF DATI" << endl;
//centroide
Double_t xcent_Dati(0.),ycent_Dati(0.);//,xcent_Dati_Fit(0.),ycent_Dati_Fit(0.); 
// array2<double> 0,ax1_0-1,0,ax2_0-1; Matrix 1,ax1_0,1,ax2_0.
xcent_Dati=get_xCentroid(Dati, ax1_0, ax2_0);
ycent_Dati=get_yCentroid(Dati, ax1_0, ax2_0);
cout << "Coordinate del centroide della PSF_DATI: " << endl;
cout<<"(xcent,ycent)= " << "(" << xcent_Dati << ","<< ycent_Dati << ")" << endl;

cout << endl;

cout << "Errore centroide DATI dal centro immagine (64,64)" << endl;
double dx,dy,rms;
dx=xcent_Dati-ax1_0/2;
dy=ycent_Dati-ax2_0/2;
rms=sqrt(pow(dx,2)+pow(dy,2));
cout << "rms_centroide= " << rms << endl;
outdata << rms << endl; 
// 
cout << endl;

fprintf(pFile,"%f %f\n",xcent_Dati,ycent_Dati);

cout << "Massimo d'intensita' dell'immagine e relative coordinate:" << endl;
Double_t Dati_max = Max(Dati);
cout << "Dati_max= " << Dati_max << endl;
outdata_1 << Dati_max << endl;
// 

//! Attenzione, NON si e' sottratto 1 (antico: 0,127 -> 1,128)
long xm,ym;
for(long i=1; i<=ax1_0;i++){
for(long j=1; j<=ax2_0;j++){
if(Dati[i][j]==Dati_max){
xm=i;
ym=j;
cout<<"Dati_(xmax,ymax)= " << "(" << xm << ","<< ym << ")" << endl;
break;
}
}
}


cout<<endl;
}
outdata.close();
outdata_1.close(); //Con ofstream non si riesce ad avere una tabella regolare di dati (2 colonne con stesso # di //stringhe)
fclose(pFile);

return 0;
}

void onedim_to_twodim(std::valarray<double> arr, array2<double> arr2, long s1, long s2){
for(long i=0;i<s1;i++){
for(long j=0;j<s2;j++){
arr2[i][j]=arr[i*s2 +j];
}
}
}

Double_t get_xCentroid(Matrix mat, long s1, long s2){
double xcent(0); 
double norma(0);
for(long i=1; i<=s1; i++){
for(long j=1; j<=s2; j++){
xcent+=i*mat[i][j];
//ycent+=j*Data[i][j];
norma+=mat[i][j];
}
}
return xcent=xcent/norma;
//ycent=ycent/norma;
}

Double_t get_yCentroid(Matrix mat, long s1, long s2){
double ycent(0); 
double norma(0);
for(long i=1; i<=s1; i++){
for(long j=1; j<=s2; j++){
//xcent+=i*Data[i][j];
ycent+=j*mat[i][j];
norma+=mat[i][j];
}
}
return ycent=ycent/norma;
//ycent=ycent/norma;
}