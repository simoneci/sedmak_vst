# Numerical characterization of the observed Point Spread Function of the VST wide-field telescope(Link) #

The numerical model of the observed Point Spread Function of the VST wide-field telescope is computed over the field of view by means of the convolution of the simulation of the atmospheric seeing and the ray-tracing, Fast-Fourier-transform-based Point Spread Function of the optical configuration of the telescope. 

The images obtained are compressed by means of two methods, a polynomial-modulated gaussian fit and a wavelet decomposition. The compressed images are mapped over the field of view by means of the interpolation of the fit or wavelet coefficients. 

The original and mapped Point Spread Function images are used for the evaluation and mapping over the field of view of various intensity and position error figures. The error maps confirm the high quality of the design of the VST telescope optics. 

Finally, the error maps can be input into the scientific processing of the astronomical observations.